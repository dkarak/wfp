<html>
<head>
	<!--Developer: Dimitris Karakostis email: dkarakostis@gmail.com  -->
    <!-- Set the title for the homepage -->
    <meta charset="UTF-8">
    <title>WFP Project</title>
    <!-- Load Ext -->
    <link rel="stylesheet" type="text/css" href="libs/ext-4.2.1.883/resources/css/ext-all.css">
    <!-- Load Ext Production -->
	<script type="text/javascript" charset="utf-8" src="libs/ext-4.2.1.883/ext-all.js"></script> 
    <!-- Load Ext Development (with debugging) -->
	<!-- <script type="text/javascript" charset="utf-8" src="../ext-4.2.1.883/ext-all-debug.js"></script> -->
    <!-- Load Openlayers-->
	<script src="libs/OpenLayers-2.12/OpenLayers.min.js"></script>
    <!-- Load our modules loader.js and map.js -->
    <script type="text/javascript" src="./loader.js"></script>
    <script type="text/javascript" src="./mapDev.js"></script>
    <!-- Googlemaps API key -->
    <script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGRfizaRoe6AXpoOPz5HRTvRmY5yFSaAE&language=el"></script>
	<!-- Jquery library-->
	<script src="libs/jquery-1.11.2.min.js"> </script> 
	<!-- Proj4js library for projection transformation-->
	<script src="libs/proj4js/lib/proj4js-compressed.js"></script> 
	<!--CSS Styles-->
	<link href="css/styles.css" rel="stylesheet">
	<!--CSS Style for Scale Line-->
	<style> 
		div.olControlScaleLine { 
			left: auto; 
			bottom:10px; 
			font-size:xx-small; 
			right:10px; 
			<!--background-color:white; --> 
			margin: 20 20 20 20; 
			<!-- border:solid white 10px; -->
		} 
	</style>

</head>
<body>
</body>
</html>
