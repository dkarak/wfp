Ext.Loader.setConfig ({
    enabled : true,
    disableCaching : false,
    paths : {
        GeoExt : "libs/geoext2-2.0.0/src/GeoExt",
        Ext : "libs/ext-4.2.1.883/src"
    }
});
