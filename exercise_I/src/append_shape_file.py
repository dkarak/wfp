
'''
Developer: Dimitris Karakostis email: dkarakostis@gmail.com
Description:
1. Gets the shape file from the Earth Observing System Data and Information System
2. Extracts the Content
3. Appends the shapefile in existing POSTGIS database
4. Creates shape files with daily hotspots for each country (only for countries that had hotspots that day).
'''

# Import libraries
import urllib2 # to get the zip file from the link
import zipfile # to unzip the file
import os
import os.path
import shutil # remove folder
import csv # read csv
import psycopg2

# Connection to local database (default username/password)
conn = psycopg2.connect("dbname='gis_exercise'host='localhost' user='postgres' password='postgres'")

# Remove existing file
shutil.rmtree('hotspot')

# Download shape file
response = urllib2.urlopen('https://firms.modaps.eosdis.nasa.gov/active_fire/shapes/zips/Global_24h.zip')
zipcontent= response.read()

# Save shapefile (in workspace directory of Eclipse)
with open("global_24.zip", 'w') as f:
    f.write(zipcontent)

# Extract shape file
unzip_shape = zipfile.ZipFile('global_24.zip')
unzip_shape.extractall('hotspot') # save unzipped shape file in folder: hotspot


# Check if file is downloaded
check_if_exists = os.path.isfile("hotspot/Global_24h.shp") # initial value is false
while check_if_exists:
    if not check_if_exists:
        print("file is not downloaded yet")
        check_if_exists = os.path.isfile("hotspot")
        continue
    else:
        # Use POSTGIS command shp2pgsql to append the data in table global_24h in PostGIS
        command = "shp2pgsql -s 4326 -a hotspot/Global_24h.shp public.hotspot | psql -h localhost -p 5432 -d gis_exercise -U postgres" 
        os.system(command)
        print ("file is downloaded")
        break


# Read the CSV file which contains all the countries (country_iso3.csv) and for each country execute a spatial query and generate a shapefile saved in the 
# folder: shapefiles

# Use POSTGIS command shp2pgsql to append the data in table global_24h in PostGIS
command = "shp2pgsql -s 4326 -d hotspot/Global_24h.shp public.daily_global | psql -h localhost -p 5432 -d gis_exercise -U postgres" 
os.system(command)

with open('country_iso3.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in reader:
        # First check if there are available data for each country
        cur = conn.cursor()
        sql = "SELECT COUNT(d.gid) FROM country AS c, daily_global AS d WHERE ST_CONTAINS(c.geom,d.geom) AND c.iso3='{}'".format(row[0])
        cur.execute(sql)
        result = cur.fetchone()
        # If there are then create shape file with the name of the country and the corresponding data
        if (result[0]>0):
            command = "pgsql2shp -f /home/enomix/workspace/gis_exercise/src/shapefiles/"+row[0]+".shp  -h localhost -u postgres -P postgres gis_exercise  \"SELECT d.latitude, d.longitude,d.brightness,d.scan,d.track,d.acq_date,d.acq_time,d.satellite,d.confidence,d.version,d.bright_t31,d.frp, d.geom FROM country AS c, daily_global AS d WHERE ST_CONTAINS(c.geom,d.geom) AND c.iso3='"+row[0]+"'""\"" 
            os.system(command)
        






