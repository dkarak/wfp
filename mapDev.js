/*
* Developer: Dimitris Karakostis email: dkarakostis@gmail.com
*/


// ADD REQUIREMENTS 
Ext.require([
    'Ext.container.Viewport', //Viewport fï¿½r ALLES
	'GeoExt.panel.Map', //Mappanel 
	'Ext.window.MessageBox', //Actions
	'GeoExt.Action', //Action
	'GeoExt.slider.LayerOpacity', //Slider
	'GeoExt.slider.Tip', //Slider Tip
	'GeoExt.slider.Zoom',  //Slider
	'Ext.data.writer.Json', //WMS
	'Ext.grid.Panel', //WMS
	'GeoExt.container.LayerLegend', //Legend
	'GeoExt.container.WmsLegend', //WMS - Legend
	'GeoExt.container.UrlLegend', // URL - Legend
	'GeoExt.container.VectorLegend', //WFSVector - Legend
	'GeoExt.panel.Legend', //Legend
	'GeoExt.data.reader.WmsCapabilities', //WMS Store
	'GeoExt.data.WmsCapabilitiesLayerStore', //WMS Store
	'GeoExt.tree.LayerLoader', //
	'GeoExt.container.WmsLegend',
	'GeoExt.tree.Panel', //Layertree:
	'Ext.data.proxy.Memory',
	'Ext.data.reader.Json',
	'Ext.layout.container.Border',
	'GeoExt.tree.Panel',
	'GeoExt.tree.OverlayLayerContainer',
	'GeoExt.tree.BaseLayerContainer',
	'GeoExt.data.LayerTreeModel',
	'GeoExt.tree.LayerLoader',
	'GeoExt.tree.View',
	'GeoExt.tree.Column',
	'GeoExt.tree.LayerNode',
	'Ext.tree.plugin.TreeViewDragDrop', //End Layertree
	'GeoExt.data.ScaleModel', //ScaleStore
	'GeoExt.data.ScaleStore',  //Scale Store
	'GeoExt.form.field.GeocoderComboBox' ,//Geocoder, Scale
	'GeoExt.form.Basic', //FormPanel - Search
	'GeoExt.window.Popup', //Popup
	'Ext.Window' ,//Popup Window
	'Ext.chart.*'  //Chart 
]);


// DEFINE PROXY.CGI URL
OpenLayers.ProxyHost = "/cgi-bin/proxy.cgi?url="; // For the pythia.telecompare.gr
//DEFINE GLOBAL VARIABLES
var map,mappanel,wms,satellite,physical,imageslayer,control,clickControl; 
var map_feature,mappanel_feature;
var popup;


// START APP
Ext.application({
    name: 'rural',
    launch: function(){
		// DEFINE PROJECTION DEFINITION  CHECK THIS SITE FOR INFO  http://www.peterrobins.co.uk/it/olchangingprojection.html
		Proj4js.defs["EPSG:2100"] = "+proj=tmerc +lat_0=0 +lon_0=24 +k=0.9996 +x_0=500000 +y_0=0 +ellps=GRS80 +towgs84=-199.87,74.79,246.62,0,0,0,0 +units=m +no_defs";
		//SET ZOOM PARAMETER FOR LOADING FEATURES
		var valueZoom =5;
        // SET MAP VARIABLE
		map = new OpenLayers.Map('map',{
                isBaseLayer: true,
                controls: []
		});
        
        // SET MAP_FEATURE VARIABLE
		map_feature = new OpenLayers.Map('map_feature',{
                isBaseLayer: false,
                controls: []
		});
 
		 
		satellite = new OpenLayers.Layer.Google(
			"Satellite View",
			{type: google.maps.MapTypeId.HYBRID,
                 numZoomLevels: 21,
				MAX_ZOOM_LEVEL: 21,
				MIN_ZOOM_LEVEL: 0}
		);
         
        
        physical = new OpenLayers.Layer.Google(
			"Physical View",
				{type: google.maps.MapTypeId.PHYSICAL,
					numZoomLevels: 21,
					MAX_ZOOM_LEVEL: 21,
					MIN_ZOOM_LEVEL: 0}
		);
 
		
        // CREATE LAYER WITH THE COUNTRIES
        wms_layer_country = new OpenLayers.Layer.WMS( "Countries", "http://localhost:8080/geoserver/gwc/service/wms", 
			{
				layers: 'un_exercise:country',
				transparent: true,
				tiled: true
			},
			{
				format: "image/png",
                visibility: true,
                displayOutsideMaxExtent: false,
				projection: new OpenLayers.Projection("EPSG:900913")
			});	
        
        
		// CREATE LAYER WITH HOTSPOTS FROM GEOSERVER	
		wms_layer_hotspots = new OpenLayers.Layer.WMS( "Hotspots", "http://localhost:8080/geoserver/gwc/service/wms", 
			{
				layers: 'un_exercise:hotspot',
				transparent: true,
				tiled: true
			},
			{
				format: "image/png",
                visibility: true,
                displayOutsideMaxExtent: false,
				projection: new OpenLayers.Projection("EPSG:900913")
        });	
    

		
		// ADD CREATED LAYERS ON THE MAP ETC.
		map.addLayers([wms_layer_hotspots,wms_layer_country,satellite]); // first you put the layer with the points
        map_feature.addLayer(physical); // first you put the layer with the points
		map.baseLayer.setOpacity(0.9); // sets the opactity of the base layers
		// SET CORRECT PROJECTION FOR LAYERS - CENTER AND ZOOM LEVEL (7)
		var lonLat = new OpenLayers.LonLat(0,0).transform(new  OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
		map.setCenter (lonLat, 1);
        var lonLat = new OpenLayers.LonLat(0,0).transform(new  OpenLayers.Projection("EPSG:4326"), map_feature.getProjectionObject());
		map_feature.setCenter (lonLat, 1);
		// ADD CONTROLS 
		map.addControl(new OpenLayers.Control.LayerSwitcher()); map_feature.addControl(new OpenLayers.Control.LayerSwitcher());
		map.addControl(new OpenLayers.Control.Navigation({'zoomBoxEnabled':false})); map_feature.addControl(new OpenLayers.Control.Navigation({'zoomBoxEnabled':false})); 
		map.addControl(new OpenLayers.Control.PanZoomBar()); map_feature.addControl(new OpenLayers.Control.PanZoomBar());
		map.addControl(new OpenLayers.Control.ArgParser());  map_feature.addControl(new OpenLayers.Control.ArgParser()); 
		map.addControl(new OpenLayers.Control.Attribution()); map_feature.addControl(new OpenLayers.Control.Attribution()); 
		map.addControl(new OpenLayers.Control.ScaleLine()); 
		
		

        valueZoom = 9;
        var wfs_layer_hotspots;
        var wfs_layer_hotspots_feature
		map.events.register("zoomend", map, function(){ // event zoomend 
			zoom = map.getZoom();
            if(zoom==valueZoom){
                
                wfs_layer_hotspots = new OpenLayers.Layer.Vector('Hotspots', {
                    visibility: true,	
                    styleMap: new OpenLayers.StyleMap({ 
							"default": new OpenLayers.Style({
								pointRadius: "6", 
								fillColor: "#FF8000",
								fillOpacity:"0.6",
								cursor: "pointer"
							},
							{ // apply a rule in order to display a name next to the station point in legend
							rules : [ 
								new OpenLayers.Rule({
									name: "Hotspots",
								})
							]
							}),
							"select": new OpenLayers.Style({
								pointRadius: "8", 
								fillColor: "#00FF00",
								fillOpacity:"1",
								cursor: "pointer"
							})
						}),				
                    strategies: [new OpenLayers.Strategy.BBOX()],
                    protocol: new OpenLayers.Protocol.WFS({
                    version: "1.1.0",
                    url: "http://localhost:8080/geoserver/wfs?", 
							featurePrefix: "un_exercise", //workspace from geoserver
							featureType: "hotspot", //layer name from geoserver  
							featureNS : "http://www.opengeospatial.net/un_exercise", //namespace from geoserver
							outputFormat: "application/json", // speeds up request/response when in this format
							readFormat: new OpenLayers.Format.GeoJSON()
						})
                });
                map.removeLayer(wms_layer_hotspots);
                map.addLayer(wfs_layer_hotspots);
                
                wfs_layer_hotspots.events.on({
                    featureselected: function(event){
                        var feature = event.feature;
                        var id = feature.attributes.gid;

                        createPopup(feature)

                        // On button click change tab and show the map with only this feature and a table with data
                        $('#btn').on( 'click',function() { // event delegation
                                var activeTab = Ext.getCmp('panel_center').getActiveTab();
                                var indexTab = Ext.getCmp('panel_center').items.indexOf(activeTab);
                                Ext.getCmp('panel_center').setActiveTab(1); // change the tab when button is clicked
                                var arr = map_feature.getLayersByName("Hotspots");
                                if (arr.length > 0) {
                                    map_feature.removeLayer(wfs_layer_hotspots_feature);
                                }
 
                                wfs_layer_hotspots_feature = new OpenLayers.Layer.Vector('Hotspots', {
                                        visibility: true,	
                                        styleMap: new OpenLayers.StyleMap({ 
                                            "default": new OpenLayers.Style({
                                                pointRadius: "6", 
                                                fillColor: "#FF8000",
                                                fillOpacity:"0.6",
                                                cursor: "pointer"
                                            },
                                            { // apply a rule in order to display a name next to the station point in legend
                                                rules : [ 
                                                    new OpenLayers.Rule({
                                                        name: "Hotspot",
                                                        filter: new OpenLayers.Filter.Comparison({
                                                        type: OpenLayers.Filter.Comparison.EQUAL_TO,
                                                        property: "gid", // name of column in table
                                                        value: id }),
                                                        symbolizer: {
                                                            graphicName: "square",
                                                            pointRadius: "7",
                                                            fillColor: "#FF8000",
                                                            strokeOpacity: "1",
                                                            strokeWidth: "1"	
                                                        }
                                                    }),
                                                ]
                                            }),
                                            "select": new OpenLayers.Style({
                                                pointRadius: "8", 
                                                fillColor: "#00FF00",
                                                fillOpacity:"1",
                                                cursor: "pointer"
                                            })
                                        }),				
                                        strategies: [new OpenLayers.Strategy.BBOX()],
                                        protocol: new OpenLayers.Protocol.WFS({
                                            version: "1.1.0",
                                            url: "http://localhost:8080/geoserver/wfs?", 
                                            featurePrefix: "un_exercise", //workspace from geoserver
                                            featureType: "hotspot", //layer name from geoserver  
                                            featureNS : "http://www.opengeospatial.net/un_exercise", //namespace from geoserver
                                            outputFormat: "application/json", // speeds up request/response when in this format
                                            readFormat: new OpenLayers.Format.GeoJSON()
                                        })
                                    });
                                    map_feature.addLayer(wfs_layer_hotspots_feature);   
                            });  
                        }
                });
                
                // Activate the feature click 
                clickControl = new OpenLayers.Control.SelectFeature([wfs_layer_hotspots],{clickout: true,});
                map.addControl(clickControl);
                clickControl.activate(); 
            }
            else if (zoom<valueZoom){
                map.removeLayer(wfs_layer_hotspots);
                map.addLayer(wms_layer_hotspots);
            }
        });
		
        
        // Function to create the popup on feature click
        function createPopup(feature) {
            
            popup = new GeoExt.Popup({
                title: "Hotspot Info Window",
                location: feature,
                width:300,
                html: "ACQ_DATE: "+feature.attributes.acq_date + "<br/>" + "ACQ_TIME: "+feature.attributes.acq_time + "<br/>" + "SATELLITE: "+feature.attributes.satellite + "<br/>" + "SCAN: " +feature.attributes.scan + "<br/>"+ "<a href='#'> <button id='btn'>Hotspot Details  </button> </a>",
                maximizable: true,
                collapsible: true,
                draggable:true
            });
            popup.show();
        }
        
		// DEFINE CONTROLS FOR PREVIOUS/NEXT FUNCTIONALITY
			
		var ctrl1, toolbarItems = [], action, actions = {};	
		/**TOOGLE HISTORY: Navigation previous, next - controls in the same toggle group*/
		ctrl1 = new OpenLayers.Control.NavigationHistory();
		map.addControl(ctrl1);
		action = Ext.create('GeoExt.Action',{
			text: "Previous",
			icon: 'libs/geoext2-2.0.0/resources/images/default/west-mini.png',
			scale: 'small', 
			control: ctrl1.previous,
			disabled: false,
			tooltip: "click button: map previous in history"
			});
		actions["previous"] = action;
		toolbarItems.push(Ext.create('Ext.button.Button', action));
		action = Ext.create('GeoExt.Action', {
			text: "Next",
			icon: 'libs/geoext2-2.0.0/resources/images/default/east-mini.png',
			scale: 'small', 
			control: ctrl1.next,
			disabled: false,
			tooltip: "click button: map next in history"
		});
		actions["next"] = action;
		toolbarItems.push(Ext.create('Ext.button.Button', action));
		toolbarItems.push("->");	
			

		
		// DEFINE THE OPACITY TOOL
		var opacity = new GeoExt.LayerOpacitySlider({
			width: 150,
			isFormField: true,
			inverse: true,
			vertical: false,
			height: 15,  //High in region
			fieldLabel: "Opacity",
			labelWidth : 42,
			tiptext: function(slider) {
				return (val );
			},
			layer: satellite,
			plugins: Ext.create("GeoExt.slider.Tip", {
				getText: function(thumb) {
					return Ext.String.format('Transparency: {0}%', thumb.value);
				}
			})
		}); 
		

	
		
		// CREATE THE MAP PANEL
        mappanel = Ext.create('GeoExt.panel.Map', {
            title: 'Map',
            map: map,
            zoom: 7,
            stateful: true,
            stateId: 'mappanel',
            bbar:[
				opacity
			]
        });
        
        // CREATE THE MAP PANEL
        mappanel_feature = Ext.create('GeoExt.panel.Map', {
            title: 'Map-Feature',
            map: map_feature,
            zoom: 7,
            stateful: true,
            stateId: 'mappanel_feature',
            bbar:[
				opacity
			]
        });
			
		
		// GET THE HEIGHT OF THE VIEWPORT (USE IT TO SET THE HEIGHT OF PANEL)
		var viewport_height = Ext.getBody().getViewSize().height;
		
		// DEFINE THE LEGEND
		var legend = Ext.create ('GeoExt.panel.Legend', {
			id: 'legend',
			bodyStyle: 'padding:10px',
			border: false,
			autowidth: true, 
			height: viewport_height,
			//style: 'padding-left:10px; padding-top:10px; padding-button:10px',
			split: true, //split for Layer
			autoScroll: true, // Slider bottom
			layerstore: map.layers, //all Layers from mapPanel automatic
			rootVisible: false, 
			dynamic: true, //default
			filter: function(record) {
				return true;
			}
		});
		
	
		
		
		
		// CREATE VIEWPORT
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            defaults: {
				collapsible: true,
				split: false,
				autoScoll: true,
				resizable: false
				
				//bodyStyle: 'padding:15px'
			},

			// REGION EAST
            items: [{
				region:'east',
                title: 'Legend',
				width: 230,
				autoHeight: true,
				items: [
                    legend
					],renderTo : Ext.getBody()
			},
				
	
			// REGION CENTER	
            {
				region: 'center', 
				title: "UN - Hotspots Map",
				layout: 'fit',
				collapsible: false,
				items: [
				{
					xtype: 'tabpanel',
					id: 'panel_center',
					activeTab: 0, // index or id
					items:[mappanel,mappanel_feature],
					
				}
				], 
				
				dockedItems: [{ 
					xtype: 'toolbar',
					dock: 'top',
					items: [
					// Previous/Next button
					{
					xtype: 'toolbar',
					items: toolbarItems
					}
					] 
				}] 
			}
			]
        });
 
    } // end launch function
}); // end ext.application
